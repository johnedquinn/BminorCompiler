/// @file:   bminor.c
/// @author: John Ed Quinn
/// @desc:   main driver of bminor compiler

/* INCLUDES */
#include "../include/token.h"
#include "../include/stmt.h"
#include "../include/library.h"
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

/* GLOBALS */
unsigned int NUM_SCAN_ERRORS = 0;
unsigned int NUM_PARSE_ERRORS = 0;
unsigned int NUM_RESOLVE_ERRORS = 0;
unsigned int NUM_TYPECHECK_ERRORS = 0;
unsigned int NUM_CODEGEN_ERRORS = 0;
unsigned int SCRATCH_COUNTER = 0;
unsigned int STRING_COUNTER = 0;
unsigned int ARG_COUNTER = 0;
unsigned int MAX_LOCAL = 0;
unsigned int COUNT_LINES = 0;

/* GLOBALS - PARSING VARS */
bool PRINT = false;

/* EXTERNS */
extern FILE *yyin;
extern int yylex();
extern char *yytext;
extern int yyparse();
extern struct stmt * parser_result;

/* PROTOTYPES */
void bminor_scan ();
void bminor_parse ();
void bminor_print ();
void bminor_resolve ();
void bminor_typecheck ();
void bminor_codegen (const char * ASSEMBLY_FNAME);
void bminor_cleanup (const char * ASSEMBLY_FNAME, const char * EXEC_FNAME);
void bminor_print_syntax_usage (FILE * stream);
void bminor_parse_command_line (int argc, char * argv[], char ** INPUT_FNAME, char ** ASSEMBLY_FNAME, char ** EXEC_FNAME);

/// @func: main
/// @desc: main driver of compiler
int main (int argc, char * argv[]) {

	// FILE NAMES
	char * INPUT_FNAME = NULL;
	char * ASSEMBLY_FNAME = NULL;
	char * EXEC_FNAME = NULL;

	// PARSE COMMAND LINE
	bminor_parse_command_line (argc, argv, &INPUT_FNAME, &ASSEMBLY_FNAME, &EXEC_FNAME);

	// OPEN INPUT FILE
	yyin = fopen(INPUT_FNAME, "r");
	if (!yyin) {
		fprintf(stderr, AC_RED "Execution Error: " AC_RESET "Could not open %s\n", argv[2]);
		return 1;
	}

	// PARSE TOKENS
	bminor_parse(INPUT_FNAME);

	// PRINT FORMATTED DOCUMENT
	if (PRINT) bminor_print();

	// PERFORM NAME RESOLUTION
	bminor_resolve();

	// PERFORM TYPECHECKING
	bminor_typecheck();

	// GENERATE ASSEMBLY CODE
	bminor_codegen(ASSEMBLY_FNAME);

	// CREATE EXECUTABLE & REMOVE ASSEMBLY
	bminor_cleanup(ASSEMBLY_FNAME, EXEC_FNAME);

	return 0;

}

/// @func: bminor_parse_command_line
/// @desc: parse command line and raise flags (if necessary)
void bminor_parse_command_line (int argc, char * argv[], char ** INPUT_FNAME, char ** ASSEMBLY_FNAME, char ** EXEC_FNAME) {
	// Need input file / arguments
	if (argc == 1) {
		fprintf(stderr, AC_RED "Syntax Error: " AC_RESET "For syntax help, run command 'bminor -h'\n");
		bminor_print_syntax_usage(stderr);
		exit(1);
	}

	unsigned int cmd_index = 1;
	while (cmd_index < argc) {
		if (!strcmp(argv[cmd_index], "-print")) PRINT = true;
		else if (!strcmp(argv[cmd_index], "-h")) {
			bminor_print_syntax_usage(stdout);
			exit(0);
		} else if (!strcmp(argv[cmd_index], "-o")) {
			*EXEC_FNAME = strdup(argv[cmd_index + 1]);
			cmd_index++;
		} else if (!*INPUT_FNAME) {
			*INPUT_FNAME = strdup(argv[cmd_index]);
		} else {
			fprintf(stderr, AC_RED "Syntax Error: " AC_RESET "Could not parse arguments. For syntax help, run command 'bminor -h'\n");
			exit(1);
		}
		cmd_index++;
	}

	// Make sure input file was passed
	if (!*INPUT_FNAME) {
		fprintf(stderr, AC_RED "Syntax Error: " AC_RESET "No input file specified\n");
		bminor_print_syntax_usage(stderr);
		exit(1);
	}

	// Check for Output File
	if (!*ASSEMBLY_FNAME) {
		*ASSEMBLY_FNAME = strdup(*INPUT_FNAME);
		strcat(*ASSEMBLY_FNAME, ".s");
	}

	// Default Executable Name (if not passed)
	if (!*EXEC_FNAME) {
		*EXEC_FNAME = strdup("a.out");
	}
}

/// @func: bminor_scan
/// @desc: scan tokens from input file
void bminor_scan () {
	while (1) {
		token_t t = yylex();
		if (t == TOKEN_EOF || t == 0) break;
		if (token_scan(t) == false) exit(1);
	}
}

/// @func: bminor_parse
/// @desc: parse input file and return the AST
void bminor_parse (const char * INPUT_FNAME) {
	if (yyparse() != 0) {
		fprintf(stderr, AC_RED "Parse Failure: " AC_RESET "Unidentified symbol '" AC_YELLOW "%s" AC_RESET "' in %s : line %d\n", yytext, INPUT_FNAME, COUNT_LINES + 1);
		exit(1);
	}
}

/// @func: bminor_print
/// @desc: prints a formatted version of the input file
void bminor_print () {
	stmt_print(parser_result, 0);
	exit(0);
}

/// @func: bminor_resolve
/// @desc: perform name resolution on input file
void bminor_resolve() {
	struct hash_table * head = NULL;
	stmt_resolve(parser_result, head);
	if (NUM_RESOLVE_ERRORS) {
		fprintf(stderr, AC_CYAN "=======> " AC_RED "Name Resolution Failed: " AC_RESET "%d resolution errors\n", NUM_RESOLVE_ERRORS);
		exit(1);
	}
}

/// @func: bminor_typecheck
/// @desc: perform typechecking on AST
void bminor_typecheck() {
	stmt_typecheck(parser_result, NULL);
	if (NUM_TYPECHECK_ERRORS) {
		fprintf(stderr, AC_CYAN "=======> " AC_RED "Typechecking Failed: " AC_RESET "%d typechecking errors\n", NUM_TYPECHECK_ERRORS);
		exit(1);
	}
}

/// @func: bminor_codegen
/// @desc: generate assembly code
void bminor_codegen(const char * ASSEMBLY_FNAME) {
	int scratch_table [6] = {0};
	FILE * OUTPUT_FILE = fopen(ASSEMBLY_FNAME, "w");
	if (!OUTPUT_FILE) {
		fprintf(stderr, AC_RED "Execution Error: " AC_RESET "Could not write to %s.\n", ASSEMBLY_FNAME);
		fclose(OUTPUT_FILE);
		exit(1);
	}
	stmt_codegen(parser_result, scratch_table, OUTPUT_FILE);
	fclose(OUTPUT_FILE);
	if (NUM_CODEGEN_ERRORS > 0) {
		fprintf(stderr, AC_CYAN "=======> " AC_RED "Assembly Code Generation Failed: " AC_RESET "%d code generation errors\n", NUM_CODEGEN_ERRORS);
		exit(1);
	}
}

/// @func: bminor_cleanup
/// @desc: compile assembly to executable and remove trash files
void bminor_cleanup (const char * ASSEMBLY_FNAME, const char * EXEC_FNAME) {
	// Compile to Executable
	char compile_buffer [256] = {0};
	sprintf(compile_buffer, "gcc -g %s library.c -o %s", ASSEMBLY_FNAME, EXEC_FNAME);
	if (system(compile_buffer) != 0) {
		fprintf(stderr, AC_RED "Linking Failed\n");
		exit(1);
	}

	// Remove Assembly File
	char removal_buffer [256] = {0};
	sprintf(removal_buffer, "rm %s", ASSEMBLY_FNAME);
	if (system(removal_buffer) != 0) {
		fprintf(stderr, AC_RED "File Garbage Collector Failure\n");
		exit(1);
	}
}

/// @func: bminor_print_syntax_usage
/// @desc: prints a syntax usage message to stdout
void bminor_print_syntax_usage (FILE * stream) {
	fprintf(stream, AC_CYAN "BMinor Syntax: " AC_RESET "bminor <arguments> <input>\n");
	fprintf(stream, "Possible Arguments:\n");
	fprintf(stream, "    -h                        : Help\n");
	fprintf(stream, "    -o <output>               : Specify Output File\n");
	fprintf(stream, "    -print                    : Print Out Formatted Input File\n");
}