# BMinor Compiler
Final Project for CSE 40243 (Compilers & Language Design - Fall 2019) at the University of Notre Dame  
Owner: Quinn, John (johnedquinn)

## Project Goal
The goal of this project is to create a compiler from scratch using C, Flex, and Bison to process BMinor files and output x86 assembly. This assembly, in turn, will be able to be assembled into an object file and then transformed into an executable.

## Background
The BMinor compiler was created to process and compile files of the [BMinor Language](https://www3.nd.edu/~dthain/courses/cse40243/fall2019/bminor.html). BMinor is a strictly-typed language whose syntax is similar to that of C. At the completion of this project, the [Original GitHub Repository](https://github.com/johnedquinn/BminorCompiler) was transferred to GitLab as a personal preference of the owner.

## To Run
In the root directory, run `make`. This will link all necessary files to create the executable compiler program, `bminor`. To use the compiler, run `./bminor <input.bminor> <arguments>`. For syntax guidance, run `./bminor -h`.  
To install the BMinor compiler to develop BMinor programs, download the project. Then, run `make`. Then, execute `pwd` in your command line to grab the absolute path to the `bminor` executable file. Copy this path. In your `~/.bashrc` file, add the following line: `export PATH=DIR:${PATH}`, where DIR is the path you copied. Close the file, and run `source ~/.bashrc`. Now, you'll be able to compile BMinor programs from any location in your computer!

## Structure of the Compiler

### Parsing
The files `decl.h`, `stmt.h`, `expr.h`, `type.h`, and `param_list.h` comprise the abstract syntax tree (AST) for BMinor. A BMinor program is a list of declarations (`decl`) of either global variables or global functions. A global function declaration is a list of statements (`stmt`) such as if-else, while-loops, and return statements. Most statements contain expressions (`expr`) which are trees of operators and values.  A type structure (`type`) is used to represent abstract types like `integer`, `string`, and `array of boolean`.

### Typechecking
The module `hash_table` implements a string-based hash table. The hash table will point to objects of type `symbol` so as to match variables names (`x`) with their definitions, like "parameter 3" or "global integer x".

### Code Generation
The module `library.c` contains the "standard library" for bminor, which is needed to implement the print statement and the exponentiation operator.