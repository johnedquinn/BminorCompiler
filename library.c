/// @file:   library.c
/// @author: John Ed Quinn
/// @desc:   defines the standard library for BMinor
/// @notes:  NA

#include "include/library.h"

/// @func: print_integer
/// @desc: prints integers
void print_integer (long x) { printf("%ld",x); }

/// @TODO: REMOVE
/// @func: print_integer_err
/// @desc: prints integer to stderr
void print_integer_err (long x) { fprintf(stderr, "%ld",x); }

/// @func: print_string
/// @desc: prints strings
void print_string (const char * s) { printf("%s",s); }

/// @func: print_boolean
/// @desc: prints true/false
void print_boolean (int b) { printf("%s",b?"true":"false"); }

/// @TODO: REMOVE
/// @func: print_boolean_err
/// @desc: prints true/false to stderr
void print_boolean_err (int b) { fprintf(stderr, "%s",b?"true":"false"); }

/// @func: print_character
/// @desc: prints characters
void print_character (char c) { printf("%c",c); }

/// @func: integer_power
/// @desc: computes integer powers
long integer_power (long x, long y) {
	long result = 1;
	while(y>0) {
		result = result * x;
		y = y -1;
	}
	return result;
}

/// @TODO: REMOVE
/// @func: print_indents
/// @desc: prints indents
void print_indents (int indent) {
	int i = 0;
	for (i; i < indent; i++) printf("\t");
}


