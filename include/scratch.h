/// @file:   scratch.h
/// @author: John Ed Quinn
/// @desc:   declares the scratch functions. Used for ...
/// ... keeping track of scratch registers
/// @notes:  NA

#ifndef SCRATCH_H
#define SCRATCH_H

/* INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include "library.h"

/* EXTERNS */
extern unsigned int SCRATCH_COUNTER;

/* FUNCTIONS */
int scratch_alloc (int scratch_table []);
void scratch_free (int scratch_table [], int r);
const char * scratch_name (int r);
int label_create ();
const char * label_name (int label);
const char * arg_name (int arg);


#endif
